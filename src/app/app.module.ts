import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { SplashScreenComponent } from "./components/splash-screen/splash-screen.component";
import { FormsModule } from "@angular/forms";
import { SearchComponent } from "./components/search/search.component";

import {
  MatTableModule,
  MatPaginatorModule,
  MatTooltipModule
} from "@angular/material";

import { NoopAnimationsModule } from "@angular/platform-browser/animations";

import { Angular2SwapiModule } from "angular2-swapi";


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SplashScreenComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    Angular2SwapiModule,
    MatTableModule,
    MatPaginatorModule,
    NoopAnimationsModule,
    MatTooltipModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
