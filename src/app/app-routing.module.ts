import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SplashScreenComponent } from "./components/splash-screen/splash-screen.component";
import { LoginComponent } from "./components/login/login.component";
import { SearchComponent } from "./components/search/search.component";

const routes: Routes = [
  { path: "", component: SplashScreenComponent },
  { path: "login", component: LoginComponent },
  { path: "search", component: SearchComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
