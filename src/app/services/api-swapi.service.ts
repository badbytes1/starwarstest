import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { throwError } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ApiSwapiService {
  swapiUrl = "https://swapi.co/api/";
  ressourcesArray: Array<Object> = new Array();
  filmsArray = new Array();
  peoplesArray = new Array();
  starshipsArray = new Array();
  vehiclesArray = new Array();
  speciesArray = new Array();
  planetsArray = new Array();

  constructor(private http: HttpClient) {}

  getDataFromApi() {
    this.getAllFilms();
    this.getAllPeoples();
    this.getAllPlanets();
    this.getAllSpecies();
    this.getAllStarships();
    this.getAllVehicles();
  }

  public getAllPeoples() {
    let obs = this.http.get(this.swapiUrl + "people?format=json");
    obs.subscribe(res => {
      this.fillArray(res, "peoples");
      this.checkIfOthersPages(res, this.swapiUrl + "people", "peoples");
    });
  }

  public getAllFilms() {
    let obs = this.http.get(this.swapiUrl + "films?format=json");
    obs.subscribe(res => {
      this.fillArray(res, "films");
      this.checkIfOthersPages(res, this.swapiUrl + "films", "films");
    });
  }

  public getAllStarships() {
    let obs = this.http.get(this.swapiUrl + "starships?format=json");
    obs.subscribe(res => {
      this.fillArray(res, "starships");
      this.checkIfOthersPages(res, this.swapiUrl + "starships", "starships");
    });
  }

  public getAllVehicles() {
    let obs = this.http.get(this.swapiUrl + "vehicles?format=json");
    obs.subscribe(res => {
      this.fillArray(res, "vehicles");
      this.checkIfOthersPages(res, this.swapiUrl + "vehicles", "vehicles");
    });
  }

  public getAllSpecies() {
    let obs = this.http.get(this.swapiUrl + "species?format=json");
    obs.subscribe(res => {
      this.fillArray(res, "species");
      this.checkIfOthersPages(res, this.swapiUrl + "species", "species");
    });
  }

  public getAllPlanets() {
    let obs = this.http.get(this.swapiUrl + "planets?format=json");
    obs.subscribe(res => {
      this.fillArray(res, "planets");
      this.checkIfOthersPages(res, this.swapiUrl + "planets", "planets");
    });
  }

  private checkIfOthersPages(res, url, arrayName) {
    if (res.next) {
      /** Determine the number of pages to fetch */
      let nbOfPages = res.count / 10;
      if (res.count % 10 != 0) {
        nbOfPages++;
      }

      for (let i = 2; i <= nbOfPages; i++) {
        this.getOthersPages(url, i, arrayName);
      }
    }
  }

  private getOthersPages(url, page, arrayName) {
    let obs = this.http.get(url + "/?page=" + page);
    obs.subscribe(res => {
      this.fillArray(res, arrayName);
    });
  }

  fillArray(res, arrayName) {
    switch (arrayName) {
      case "peoples": {
        this.peoplesArray.push(...res.results);
        //statements;
        break;
      }
      case "films": {
        this.filmsArray.push(...res.results);
        //statements;
        break;
      }
      case "starships": {
        this.starshipsArray.push(...res.results);
        //statements;
        break;
      }
      case "vehicles": {
        this.vehiclesArray.push(...res.results);
        //statements;
        break;
      }
      case "species": {
        this.speciesArray.push(...res.results);
        //statements;
        break;
      }
      case "planets": {
        this.planetsArray.push(...res.results);
        //statements;
        break;
      }
      default: {
        //statements;
        break;
      }
    }
  }

  getArrayPeoples() {
    return this.peoplesArray;
  }

  getArrayFilms() {
    return this.filmsArray;
  }

  getArrayStarships() {
    return this.starshipsArray;
  }

  getArrayVehicles() {
    return this.vehiclesArray;
  }

  getArraySpecies() {
    return this.speciesArray;
  }

  getArrayPlanets() {
    return this.planetsArray;
  }
}
