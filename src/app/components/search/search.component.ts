import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { MatPaginator, MatTableDataSource, MatSort } from "@angular/material";
import { ApiSwapiService } from "../../services/api-swapi.service";


@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  selectedFilter: string = "films";
  ressourcesArray: Array<Object> = new Array();
  searchText: string = "";
  datasource: MatTableDataSource<any> = new MatTableDataSource<any>();
  displayedColumnsPeoples = ["name", "weight", "height", "hair_color"];
  displayedColumnsFilms = ["title", "director", "producer", "release_date"];
  displayedColumnsVehiclesStarships = [
    "name",
    "model",
    "length",
    "manufacturer"
  ];
  displayedColumnsSpecies = [
    "name",
    "language",
    "average_height",
    "average_lifespan"
  ];
  displayedColumnsPlanets = ["name", "population", "terrain", "climate"];

  closeResult: string;

  constructor(
    private apiservice: ApiSwapiService
  ) {}

  ngOnInit() {}

  ngAfterViewInit() {
    if (this.apiservice.getArrayFilms().length == 0) {
      this.apiservice.getDataFromApi();
    } else {
      this.getData("films");
    }
  }

  getData(selectedValue) {
    this.searchText = "";
    this.selectedFilter = selectedValue;
    switch (selectedValue) {
      case "peoples": {
        this.ressourcesArray = this.apiservice.getArrayPeoples();
        //statements;
        break;
      }
      case "films": {
        this.ressourcesArray = this.apiservice.getArrayFilms();
        //statements;
        break;
      }
      case "starships": {
        this.ressourcesArray = this.apiservice.getArrayStarships();
        //statements;
        break;
      }
      case "vehicles": {
        this.ressourcesArray = this.apiservice.getArrayVehicles();
        //statements;
        break;
      }
      case "species": {
        this.ressourcesArray = this.apiservice.getArraySpecies();
        //statements;
        break;
      }
      case "planets": {
        this.ressourcesArray = this.apiservice.getArrayPlanets();
        //statements;
        break;
      }
      default: {
        //statements;
        break;
      }
    }
    this.datasource = new MatTableDataSource<any>(this.ressourcesArray);
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
    this.datasource.paginator.firstPage();
  }

  filterResults() {
    this.datasource.filter = this.searchText.trim().toLowerCase();
  }

  getToolTipData(row) {
    return row;
  }
}
