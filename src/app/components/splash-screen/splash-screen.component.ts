import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiSwapiService } from "../../services/api-swapi.service";

@Component({
  selector: "app-splash-screen",
  templateUrl: "./splash-screen.component.html",
  styleUrls: ["./splash-screen.component.scss"]
})
export class SplashScreenComponent implements OnInit {
  constructor(private router: Router, private apiService: ApiSwapiService) {}

  ngOnInit() {
    /*Load all Star Wars Ressources on splash screen execution*/
    this.apiService.getDataFromApi();
  }

  skipToLogin() {
    this.router.navigate(["/login"]);
  }
}
