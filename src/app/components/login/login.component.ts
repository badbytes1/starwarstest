import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  constructor(private router: Router) {}

  username: string;
  password: string;
  wrongCredentials: boolean = false;

  ngOnInit() {}

  login(): void {
    /**Simulate a code encryption password Code Base 64  */
    if (this.username == "luke" && btoa(this.password) == "bHVrZQ==") {
      this.router.navigate(["/search"]);
    } else {
      this.wrongCredentials = true;
    }
  }
}
